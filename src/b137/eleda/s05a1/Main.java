package b137.eleda.s05a1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setNumbers("999695342");
        contact1.setAddresses("Manila");

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setNumbers("9994535235");
        contact2.setAddresses("Quezon");

        Phonebook phonebook = new Phonebook();
        phonebook.add(contact1);
        phonebook.add(contact2);

        phonebook.showDetails();
        
    }
}
