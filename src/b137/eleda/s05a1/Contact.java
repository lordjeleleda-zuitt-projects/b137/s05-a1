package b137.eleda.s05a1;

import java.util.ArrayList;

public class Contact {

    // Properties
    private String name;

    private ArrayList<String> numbers = new ArrayList<String>();

    private ArrayList<String> addresses = new ArrayList<String>();

    // Constructor

    // Empty constructor
    public Contact() {}

    // Parameterized constructor
    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    // Getters & Setters
    public String getName() {

        return name;
    }

    public ArrayList<String> getNumbers() {

        return numbers;
    }

    public ArrayList<String> getAddresses() {

        return addresses;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setNumbers(ArrayList<String> newNumbers) {
        this.numbers = newNumbers;
    }

    public void setAddresses(ArrayList<String> newAddresses) {
        this.addresses = newAddresses;
    }

    //Methods
    public void showDetails() {
        System.out.println(this.name);
        System.out.println(this.name + " has the following registered numbers:\n " + this.numbers);
        System.out.println(this.name + " has the following registered numbers:\n " + this.addresses);
    }

}
