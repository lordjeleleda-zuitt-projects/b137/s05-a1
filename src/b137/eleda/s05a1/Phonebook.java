package b137.eleda.s05a1;

import java.util.ArrayList;

public class Phonebook extends Contact {

    // Properties
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Constructor

    // Empty constructor
    public Phonebook() {
        super();
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getters & Setters
    public ArrayList<Contact> getContacts() {

        return contacts;
    }

    public void setContacts(ArrayList<Contact> newContacts) {
        this.contacts = newContacts;
    }

    // Methods
    public void showDetails() {
        System.out.println(super.getName());
        System.out.println(super.getName() + " has the following registered numbers:\n " + super.getNumbers());
        System.out.println(super.getName() + " has the following registered addresses:\n " + super.getAddresses());
    }

}
